package model;

public class Paciente extends Usuario {
	private String doenca; 
	public Paciente(String nome) {
		super();
		super.nome = nome;
	}
	public String getDoenca() {
		return doenca;
	}
	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}
	

}
