package model;

public class Medico extends Usuario{

	private String especialidade;
	private String codigoInscricao;
	
	public Medico(String especialidade){
		super();
		this.especialidade = especialidade;
	}
	public String getEspecialidade() {
		return especialidade;
	}
	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}
	public String getCodigoInscricao() {
		return codigoInscricao;
	}
	public void setCodigoInscricao(String codigoInscricao) {
		this.codigoInscricao = codigoInscricao;
	}
	
}
