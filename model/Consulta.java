package model;

public class Consulta {
	private String data;
	private String horario;
	private String sala;
	private String codigo;
	private Medico umMedico;
	private Paciente umPaciente;
	
	public Consulta(String codigo){
		this.codigo = codigo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Medico getUmMedico() {
		return umMedico;
	}
	public void setUmMedico(Medico umMedico) {
		this.umMedico = umMedico;
	}
	public Paciente getUmPaciente() {
		return umPaciente;
	}
	public void setUmPaciente(Paciente umPaciente) {
		this.umPaciente = umPaciente;
	}
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getSala() {
		return sala;
	}

	public void setSala(String sala) {
		this.sala = sala;
	}

}
