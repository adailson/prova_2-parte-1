package model;

public class Usuario {
	public String nome;
	private String cpf;
	private String telefone;
	//private Endereco endereco;
	//private Consulta consulta;
	
	public Usuario(String nome){
		this.nome = nome;
	}
	public Usuario(){
		super();
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}
