package controller;

import java.util.ArrayList;
import model.*;

public class ControladorConsulta {
	ArrayList<Consulta> listaConsulta;
	public ControladorConsulta() {
		listaConsulta = new ArrayList<Consulta>();
	}
	public void adicionar(Consulta umaConsulta){
		listaConsulta.add(umaConsulta);
	}
	public void remover(Consulta umaConsulta){
		listaConsulta.remove(umaConsulta);
	}
	public Consulta buscar(String codigo){
		for(Consulta umaConsulta: listaConsulta){
			if(umaConsulta.getCodigo().equalsIgnoreCase(codigo)){
				return umaConsulta;
			}
		}
		return null;
	}

}
