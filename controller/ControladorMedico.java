package controller;

import java.util.ArrayList;
import model.*;

public class ControladorMedico {
	ArrayList<Medico> listaMedico;
	public ControladorMedico() {
		listaMedico = new ArrayList<Medico>();
	}
	public void adicionar(Medico umMedico){
		listaMedico.add(umMedico);
	}
	public void remover(Medico umMedico){
		listaMedico.remove(umMedico);
	}
	public Medico buscar(String nome){
		for(Medico umMedico: listaMedico){
			if(umMedico.getNome().equalsIgnoreCase(nome)){
				return umMedico;
			}
		}
		return null;
	}

}
