package controller;

import java.util.ArrayList;
import model.*;

public class ControladorPaciente {
	ArrayList<Paciente> listaPaciente;
	public ControladorPaciente() {
		listaPaciente = new ArrayList<Paciente>();
	}
	public void adicionar(Paciente umPaciente){
		listaPaciente.add(umPaciente);
	}
	public void remover(Paciente umPaciente){
		listaPaciente.remove(umPaciente);
	}
	public Paciente buscar(String nome){
		for(Paciente umPaciente: listaPaciente){
			if(umPaciente.getNome().equalsIgnoreCase(nome)){
				return umPaciente;
			}
		}
		return null;
	}

}
