package controller;

import java.util.ArrayList;
import model.*;

public class ControladorUsuario {
	ArrayList<Usuario> listaUsuario;
	public ControladorUsuario() {
		listaUsuario = new ArrayList<Usuario>();
	}
	public void adicionar(Usuario umUsuario){
		listaUsuario.add(umUsuario);
	}
	public void remover(Usuario umUsuario){
		listaUsuario.remove(umUsuario);
	}
	public Usuario buscar(String nome){
		for(Usuario umUsuario: listaUsuario){
			if(umUsuario.getNome().equalsIgnoreCase(nome)){
				return umUsuario;
			}
		}
		return null;
	}

}
